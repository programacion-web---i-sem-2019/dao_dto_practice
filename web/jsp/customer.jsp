<%@page import="Modelo.CustomerDTO"%>
<%@page import="java.util.List"%>
<%@page import="Persistencia.CustomerDAO"%>
<!DOCTYPE html>
<html lang="es" dir="ltr">
    <head>
    <meta charset="utf-8">
    <title>DAO & DTO</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    </head>
        <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <div class="navbar-header">
                    <h1>DAO & DTO</h1>
                </div>
            </div>
        </nav>
        <br>
        <div class="container">
            <br>
            <h1>CLIENTES</h1>
            <!-- Tabla -->
            <div class="col-md-12">
                <table class="table table-bordered table-hover bg-light">
                    <thead>
                        <tr>
                            <th>N�</th>
                            <th>Nombre</th>
                            <th>Pa�s</th>
                            <th>Direcci�n</th>
                            <th>C�digo Postal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% 
                            CustomerDAO dao = new CustomerDAO();
                            List<CustomerDTO> cu = dao.readCustomer();
                            for(CustomerDTO now: cu){
                        %>
                        <tr>
                            <td><%=now.getId() %></td>
                            <td width="30%"><%=now.getFullName() %></td>
                            <td width="20%"><%=now.getCountry() %></td>
                            <td><%=now.getAddress() %></td>                                        
                            <td><%=now.getZipCode() %></td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
            </div>
            <a href="../index.html" class="btn btn-primary">Volver</a>
        </div>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</html>