<%@page import="Modelo.Alumno"%>
<%@page import="java.util.List"%>
<%@page import="Persistencia.AlumnoDAO"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <title>DAO & DTO</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link rel="stylesheet" href="css/styles.css"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <div class="navbar-header">
                    <h1>DAO & DTO</h1>
                </div>
            </div>
        </nav>
        <br>
        <!-- Registro -->
        <div class="container">
            <h1>ESTUDIANTES</h1>
            <div class="row mt-5">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <form action="Update.do">
                                <%
                                    String ced = request.getSession().getAttribute("ced").toString();
                                    String nombre = request.getSession().getAttribute("name").toString();
                                    String apellido = request.getSession().getAttribute("ape").toString();
                                %>
                                <div class="form-group">
                                    <input type="text" name="ced" class="form-control" value="<%=ced %>" required>
                                </div>
                                <br>
                                <div class="form-group">
                                    <input type="text" name="nombre" value="<%=nombre %>" class="form-control">
                                </div>
                                <br>
                                <div class="form-group">
                                    <input type="text" name="apellido" value="<%=apellido %>" class="form-control">
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary">Actualizar Estudiante</button>
                            </form>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>

                                <br>
        <footer>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-6 offset-4">
                        <h5>
                            Harold Rueda - 1151904
                            | Jefferson Pallares - 1151508 <br>
                            UFPS-2021-Programación web - UFPS</h5>
                    </div>                    
                </div>
            </div>
        </footer>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</html>