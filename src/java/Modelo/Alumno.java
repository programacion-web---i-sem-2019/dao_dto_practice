/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Harold
 */
@Entity
public class Alumno implements Serializable {
    
    @Id
    private String ced;
    @Basic
    private String nombre;
    private String apellido;
    
    public Alumno() {
    }

    public Alumno(String ced, String nombre, String apellido) {
        this.ced = ced;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getCed() {
        return ced;
    }

    public void setCed(String ced) {
        this.ced = ced;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }   
    
}
