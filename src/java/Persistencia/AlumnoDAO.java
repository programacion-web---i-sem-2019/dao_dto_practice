/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Modelo.Alumno;
import Persistencia.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Harold
 */
public class AlumnoDAO {
    AlumnoJpaController alu = new AlumnoJpaController();
    
    public void create(Alumno a){
        try {
            alu.create(a);
        } catch (Exception ex) {
            Logger.getLogger(AlumnoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Alumno> read(){
        return alu.findAlumnoEntities();
    }
    
    public Alumno readAlumno(String ced){
        return alu.findAlumno(ced);
    }
    
    public void update(Alumno a){
        try {
            alu.edit(a);
        } catch (Exception ex) {
            Logger.getLogger(AlumnoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void delete(String ced){
        try {
            alu.destroy(ced);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(AlumnoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
